using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Misil : MonoBehaviour
{

    Vector3 objetivo;

    public void MarcarObjetivo(Vector3 objetivoMarcado)
    {
        objetivo = objetivoMarcado;
    }

    void Update()
    {
        transform.LookAt(objetivo);
        transform.Translate(Vector3.forward * Time.deltaTime * 2);
    
        if(Vector3.Distance(transform.position, objetivo) < 0.1F)
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            //collision.gameObject.GetComponentInParent<Jugador>().Golpeado();
        }
        Destroy(gameObject);
    }
}
