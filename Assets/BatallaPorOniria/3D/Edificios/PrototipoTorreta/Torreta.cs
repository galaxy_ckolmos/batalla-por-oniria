using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Torreta : MonoBehaviour
{
    // Prefab que vamos a instanciar
    public GameObject prefabDisparo;

    // Jugador
    Vector3 objetivo;
    
    // Eje de rotaci�n del ca��n
    Transform eje;

    // Ca��n (para la posici�n de la instancia
    Transform ca�on;

    // Booleana de control para saber si el jugador est� a la vista
    bool objetivoFijado = false;

    /// <summary>
    /// Inicializar variables y llamar corrutinas
    /// </summary>
    private void Start()
    {
        // Rellena las variables eje y ca��n
        eje = transform.GetChild(0).GetChild(0);
        ca�on = eje.GetChild(0);

        // Llama a la corrutina Disparo
        StartCoroutine(Disparo());
    }

    private void Update()
    {
        // MIRAR HACIA UN OBJETIVO INTERPOLANDO LA ROTACI�N 

        // Calcula una rotaci�n basada en la posici�n del objetivo, con respecto a la posici�n del eje
        Quaternion rotacionFinal = Quaternion.LookRotation(objetivo - eje.transform.position);

        // Altera la rotaci�n del eje SUAVEMENTE (interpola)
        eje.transform.rotation = Quaternion.Slerp(eje.transform.rotation, rotacionFinal, Time.deltaTime*5);
    }

    // Las corrutinas permiten dejar pasar tiempo. Su tipo es IEnumerator
    IEnumerator Disparo()
    {
        //Bucle infinito PERO controlado gracias a la corrutina
        while (true)
        {
            // Si estamos viendo al jugador
            if (objetivoFijado)
            {
                // Instanciamos un disparo
                GameObject instancia = Instantiate(prefabDisparo, ca�on.position, Quaternion.identity);
                instancia.GetComponent<Misil>().MarcarObjetivo(objetivo);
            
            }

            // Esperamos un segundo
            yield return new WaitForSeconds(1);
        }
    }

    // Se dispara todos los fotogramas en los que detecte colisi�n
    void OnTriggerStay(Collider collider)
    {
        // Comprobamos si es el jugador el que ha hecho saltar el trigger
        if(collider.tag == "Player")
        {
            // Rellenamos el objetivo al que apuntar
            objetivo = collider.transform.position+Vector3.up; // Sumamos Vector3.up para no apuntar a los pies del objetivo

            RaycastHit hit; // Variable con informaci�n del choque
            Ray rayo = new Ray(ca�on.position, ca�on.up); // Usamos up como direcci�n porque el cilindro del rat�n apunta a su eje Y

            // dibujamos el rayo en la escena
            Debug.DrawRay(ca�on.position, ca�on.up * 10, Color.red);

            // Lanzamos el Raycast con distancia 10
            bool impacto = Physics.Raycast(rayo, out hit, 10);

            // Si el rayo impacta
            if (impacto)
            {
                // Si ha impactado contra el jugador
                if (hit.collider.tag == "Player")
                {
                    objetivoFijado = true; // La variable nos sirve para controlar si estamos viendo al jugador
                }
                else
                {
                    objetivoFijado = false; // No estamos viendo al jugador
                }

            }

        }
    }

    private void OnTriggerExit(Collider other)
    {
        // Hemos dejado de ver al jugador
        objetivoFijado = false;
    }
}
