using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneer : MonoBehaviour
{
    public void CargarTripleEstrellas()
    {
        SceneManager.LoadScene(1);
    }
    public void CargarOniricos()
    {
        SceneManager.LoadScene(2);
    }
    
    public void CargarAmbos()
    {
        SceneManager.LoadScene(3);
    }

    public void Salir()
    {
        Application.Quit();
    }
}
